import pygame

from game_object import Graphics, GameObject


class TextureGraphics(Graphics):
    STATE_IDLE = 0
    STATE_RUN = 1

    FRAME_DELAY = 100

    def __init__(self, textures):
        self.textures = textures
        self.sprite_num = 0
        self.texture = textures.idle
        self.state = self.STATE_IDLE
        self.start_time = pygame.time.get_ticks()
        self.flip_texture = False

    def update(self, game_object: GameObject):
        if self.state == self.STATE_IDLE:
            self.on_idle(game_object)
        elif self.state == self.STATE_RUN:
            self.on_running(game_object)

        ticks = pygame.time.get_ticks()
        if ticks - self.start_time >= self.FRAME_DELAY:
            self.start_time = ticks
            self.sprite_num = (self.sprite_num + 1) % self.texture.sprites_count

    def on_idle(self, game_object: GameObject):
        if game_object.force_direction.x != 0 or game_object.force_direction.y != 0:
            self.change_state(self.STATE_RUN)

    def on_running(self, game_object: GameObject):
        if game_object.velocity.x == 0 and game_object.velocity.y == 0:
            self.change_state(self.STATE_IDLE)
        elif game_object.force_direction.x != 0:
            self.flip_texture = game_object.force_direction.x < 0

    def change_state(self, state):
        self.start_time = pygame.time.get_ticks()
        self.state = state
        self.sprite_num = 0
        if state == self.STATE_IDLE:
            self.texture = self.textures.idle
        elif state == self.STATE_RUN:
            self.texture = self.textures.running

    def render(self, game_object: GameObject, screen: pygame.Surface):
        sprite = self.texture.get_sprite(self.sprite_num, self.flip_texture)
        screen.blit(sprite, game_object.position + self.textures.offset)
