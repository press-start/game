from collections import namedtuple

from game_object import *

PlayerInputParams = namedtuple('PlayerInputParams', 'key_left key_right key_up key_down')


class PlayerInput(Input):
    def __init__(self, params: PlayerInputParams):
        self.params = params

    def update(self, game_object: GameObject, world):
        pressed_keys = pygame.key.get_pressed()
        game_object.force_direction.x = pressed_keys[self.params.key_right] - pressed_keys[self.params.key_left]
        game_object.force_direction.y = pressed_keys[self.params.key_down] - pressed_keys[self.params.key_up]
        if game_object.force_direction.length() > 0:
            game_object.force_direction.normalize_ip()
