import pygame

from game_object import Physics, GameObject


class SimplePhysics(Physics):
    DRAG_COEFFICIENT = 0.1
    STATIC_FRICTION = 5

    def update(self, game_object: GameObject, world):
        force = game_object.force_direction * game_object.strength
        current_velocity = game_object.velocity.length()
        if current_velocity != 0:
            if game_object.force_direction.length() == 0 and current_velocity < self.STATIC_FRICTION:
                game_object.velocity.scale_to_length(0)
            else:
                drag = -game_object.velocity.normalize()
                drag.scale_to_length(self.DRAG_COEFFICIENT * current_velocity * current_velocity)
                force += drag

        game_object.velocity += force / game_object.mass
        game_object.position += game_object.velocity
        self.solve_collisions(game_object, world)
        self.bound_to_world(game_object, world)

    @staticmethod
    def bound_to_world(game_object, world):
        world_bounds = pygame.Rect(world.bounds)
        object_bounds = pygame.Rect(game_object.position, game_object.size)
        object_bounds.clamp_ip(world_bounds)
        game_object.position.x = object_bounds.x
        game_object.position.y = object_bounds.y

    def solve_collisions(self, game_object, world):
        ob1 = game_object
        for ob2 in world.objects:
            if ob1 != ob2 and ob1.bounds.colliderect(ob2.bounds):
                rect1: pygame.Rect = ob1.bounds
                rect2: pygame.Rect = ob2.bounds
                cx1, cy1 = rect1.center
                cx2, cy2 = rect2.center
                dx = (rect1.width + rect2.width) / 2 - abs(cx1 - cx2)
                dy = (rect1.height + rect2.height) / 2 - abs(cy1 - cy2)
                mass_sum = ob1.mass + ob2.mass
                if dx < dy:
                    sign = 1 if cx1 > cx2 else -1
                    ob1.position.x += sign * dx / mass_sum * ob2.mass
                    ob2.position.x -= sign * dx / mass_sum * ob1.mass
                else:
                    sign = 1 if cy1 > cy2 else -1
                    ob1.position.y += sign * dy / mass_sum * ob2.mass
                    ob2.position.y -= sign * dy / mass_sum * ob1.mass
