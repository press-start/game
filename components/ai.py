import pygame
from pygame import *

from game_object import Input, GameObject


class FollowPlayerAI(Input):
    def update(self, game_object: GameObject, world):
        from entities import EntityType
        target = world.find_nearby(game_object.position, entity_type=EntityType.PLAYER)
        distance = game_object.position.distance_to(target.position)
        if distance <= 16:
            exit(0)
        game_object.force_direction = calc_direction(game_object.position, target.position)


class ChargeToPlayerAI(Input):
    STATE_WAITING = 0
    STATE_RUNNING = 1

    WAITING_DURATION = 1 * 1000
    WALL_DISTANCE = 10

    def __init__(self):
        self.state = self.STATE_WAITING
        self.start_tick = 0

    def update(self, game_object: GameObject, world):
        if self.start_tick == 0:
            self.start_tick = pygame.time.get_ticks()

        if self.state == self.STATE_WAITING:
            self.wait(game_object, world)
        elif self.state == self.STATE_RUNNING:
            self.run(game_object, world)

    def wait(self, game_object, world):
        if pygame.time.get_ticks() - self.start_tick > self.WAITING_DURATION:
            from entities import EntityType
            target = world.find_nearby(game_object.position, EntityType.PLAYER)
            game_object.force_direction = calc_direction(game_object.position, target.position)
            self.state = self.STATE_RUNNING

    def run(self, game_object, world):
        bounds: pygame.Rect = game_object.bounds
        world_bounds = pygame.Rect(world.bounds)
        direction = game_object.force_direction
        if bounds.top - world_bounds.top < self.WALL_DISTANCE and direction.y < 0 \
                or bounds.left - world_bounds.left < self.WALL_DISTANCE and direction.x < 0 \
                or world_bounds.right - bounds.right < self.WALL_DISTANCE and direction.x > 0 \
                or world_bounds.bottom - bounds.bottom < self.WALL_DISTANCE and direction.y > 0:
            self.start_tick = 0
            game_object.force_direction.scale_to_length(0)
            self.state = self.STATE_WAITING


def calc_direction(from_pos: pygame.Vector2, to_pos: pygame.Vector2) -> pygame.Vector2:
    direction = to_pos - from_pos
    if direction.length() != 0:
        direction.normalize_ip()
    return direction
