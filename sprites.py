from typing import List

import pygame

from entities import EntityType


class SpriteSheet:
    TEXTURES_SIZE = 16

    def __init__(self, filename: str, scale: int):
        try:
            sheet = pygame.image.load(filename).convert_alpha()
            width, height = sheet.get_size()
            self.sheet = pygame.transform.scale(
                sheet, (width * scale, height * scale)
            )
            self.scale = scale
        except pygame.error as message:
            print('Unable to load sprite sheet image:', filename, f"because of {message}")
            exit(1)

    def image_at(self, rect) -> pygame.Surface:
        """Loads image from rectangle"""
        x, y, width, height = (x * self.scale for x in rect)
        image = pygame.Surface((width, height), pygame.SRCALPHA)
        image.blit(self.sheet, (0, 0), (x, y, width, height))
        return image

    def image_at_slot(self, rect) -> pygame.Surface:
        """Loads image from slots specified in rect"""
        return self.image_at((x * self.TEXTURES_SIZE for x in rect))

    def images_at_slots(self, rects) -> List[pygame.Surface]:
        """Loads multiple images, supply a list of coordinates"""
        return [self.image_at_slot(rect) for rect in rects]

    def load_strip(self, bounds, image_count) -> List[pygame.Surface]:
        """Loads animation frames and returns them as a list"""
        x, y, width, height = bounds
        rects = [(x + width * i, y, width, height) for i in range(image_count)]
        return self.images_at_slots(rects)


class AnimatedTexture:
    def __init__(self, sprites):
        self.sprites_count = len(sprites)
        self.sprites = [
            (s, pygame.transform.flip(s, True, False)) for s in sprites
        ]

    def get_sprite(self, sprite_num, flip=False):
        return self.sprites[sprite_num][flip]


class PlayerTextures:
    def __init__(self, sheet: SpriteSheet):
        entity_type = EntityType.PLAYER
        sprites = sheet.load_strip(entity_type.texture_bounds, image_count=7)
        self.idle = AnimatedTexture(sprites[:2])
        self.running = AnimatedTexture(sprites[2:6])
        self.beat = AnimatedTexture(sprites[-1:])
        self.offset = calculate_offset(sprites[0].get_size(), entity_type.size)


class EntityTextures:
    def __init__(self, sheet: SpriteSheet, entity_type: EntityType):
        sprites = sheet.load_strip(entity_type.texture_bounds, image_count=4)
        self.idle = AnimatedTexture(sprites[-1:])
        self.running = AnimatedTexture(sprites)
        self.offset = calculate_offset(sprites[0].get_size(), entity_type.size)


class TextureManager:
    def __init__(self, sprite_sheet="res/spritesheet.png", map_sheet="res/map.png", scale=4):
        self.sheet = SpriteSheet(sprite_sheet, scale)
        self.textures = {
            entity_type: PlayerTextures(self.sheet) if entity_type == EntityType.PLAYER
            else EntityTextures(self.sheet, entity_type)
            for entity_type in EntityType
        }
        map_sheet = SpriteSheet(map_sheet, scale)
        self.map = map_sheet.image_at_slot((0, 0, 15, 8))

    def get_entity_textures(self, entity_type):
        return self.textures[entity_type]


def calculate_offset(texture_size, box_size):
    width1, height1 = texture_size
    width2, height2 = box_size
    offset_x = (width2 - width1) // 2
    offset_y = height2 - height1
    return offset_x, offset_y
