from typing import List

from entities import EntityType, PlayerInputParams
from game_object import *
from locals import texture_manager


class World:
    def __init__(self, bounds):
        self.objects: List[GameObject] = []
        self.players: List[GameObject] = []
        self.bounds = bounds

    def update(self):
        for obj in self.objects:
            obj.update(self)
        self.objects.sort(key=lambda o: o.bounds.bottom)

    def render(self, screen: pygame.Surface):
        screen.blit(texture_manager.var.map, (0, 0))
        for obj in self.objects:
            obj.render(screen)

    def spawn_player(self, position):
        if len(self.players) == 0:
            params = PlayerInputParams(pygame.K_a, pygame.K_d, pygame.K_w, pygame.K_s)
        else:
            params = PlayerInputParams(pygame.K_LEFT, pygame.K_RIGHT, pygame.K_UP, pygame.K_DOWN)
        player = self.spawn_object(EntityType.PLAYER, position, params)
        self.players.append(player)

    def spawn_object(self, entity_type: EntityType, position, params=None) -> GameObject:
        game_object = entity_type.create(params)
        game_object.position.x, game_object.position.y = position
        self.objects.append(game_object)
        return game_object

    def find_nearby(self, position: pygame.Vector2, entity_type: EntityType) -> GameObject:
        target: GameObject = None
        distance = 32000
        for o in self.objects:
            if o.type == entity_type:
                curr_distance = position.distance_to(o.position)
                if curr_distance < distance:
                    target = o
                    distance = curr_distance
        return target
