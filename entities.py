from enum import Enum

from components import *
from components.ai import FollowPlayerAI, ChargeToPlayerAI
from components.physics import SimplePhysics
from game_object import GameObject
from locals import texture_manager


class EntityType(Enum):
    def __init__(self, mass, strength, size, input_comp, physics_comp, graphics_comp, sprite_bounds):
        self.mass = mass
        self.strength = strength
        self.size = size
        self.input_comp = input_comp
        self.physics_comp = physics_comp
        self.graphics_comp = graphics_comp
        self.texture_bounds = sprite_bounds

    PLAYER = 5, 6, (24, 48), PlayerInput, SimplePhysics, TextureGraphics, (0, 0, 1, 2)
    DRONE = 2, 2, (32, 44), FollowPlayerAI, SimplePhysics, TextureGraphics, (0, 2, 1, 1)
    CHICKEN = 4, 8, (28, 44), ChargeToPlayerAI, SimplePhysics, TextureGraphics, (0, 3, 1, 1)

    def create(self, params=None) -> GameObject:
        if self.graphics_comp == TextureGraphics:
            textures = texture_manager.var.get_entity_textures(self)
            graphics_comp = TextureGraphics(textures)
        else:
            graphics_comp = self.graphics_comp()

        if self.input_comp == PlayerInput:
            input_comp = PlayerInput(params)
        else:
            input_comp = self.input_comp()

        game_object = GameObject(self, input_comp, self.physics_comp(), graphics_comp)
        return game_object
