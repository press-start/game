import pygame


class GameObject:
    def __init__(self, entity_type, input_comp, physics_comp, graphics_comp):
        self.type = entity_type
        self.position = pygame.Vector2(0, 0)
        self.velocity = pygame.Vector2(0, 0)
        self.size = entity_type.size
        self.mass = entity_type.mass
        self.strength = entity_type.strength
        self.force_direction = pygame.Vector2(0, 0)
        self.input_comp = input_comp
        self.physics_comp = physics_comp
        self.graphics_comp = graphics_comp

    @property
    def bounds(self) -> pygame.Rect:
        return pygame.Rect(self.position, self.size)

    def update(self, world):
        self.input_comp.update(self, world)
        self.physics_comp.update(self, world)
        self.graphics_comp.update(self)

    def render(self, screen):
        self.graphics_comp.render(self, screen)


class Input:
    def update(self, game_object: GameObject, world):
        pass


class Physics:
    def update(self, game_object: GameObject, world):
        pass


class Graphics:

    BLOCK = 16

    def update(self, game_object: GameObject):
        pass

    def render(self, game_object: GameObject, screen: pygame.Surface):
        pass
