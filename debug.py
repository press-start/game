import pygame
from game_object import *
from typing import List


class DebugGUI:
    def __init__(self, clock: pygame.time.Clock, player: GameObject):
        self.clock = clock
        self.font = pygame.font.Font(None, 18)
        self.player_pos = player.position

    def render(self, screen: pygame.Surface):
        fps = int(self.clock.get_fps())
        fps_surface = self.font.render(f'FPS: {fps} | X: {self.player_pos.x} | Y: {self.player_pos.y}', False, pygame.Color('green'))
        screen.blit(fps_surface, (0, 0))
