import pygame

import debug
from entities import EntityType
from locals import texture_manager
from sprites import TextureManager
from world import World


class Game:
    def __init__(self, size):
        pygame.init()
        self.screen = pygame.display.set_mode(size)
        self.clock = pygame.time.Clock()
        self.running = True

        self.world = World((64, 64, 832, 384))

        texture_manager.var = TextureManager()

        cx, cy = (x / 2 for x in size)
        self.world.spawn_player((cx, cy))
        self.world.spawn_player((cx, cy))
        self.world.spawn_object(EntityType.DRONE, (250, 250))
        self.world.spawn_object(EntityType.CHICKEN, (300, 300))

        self.fps_render = debug.DebugGUI(self.clock, self.world.objects[0])
        self.fps_render.font.set_bold(True)

        pygame.mouse.set_visible(True)

    def loop(self):
        while self.running:
            self.clock.tick(60)
            self.process_input()
            self.update()
            self.render()

    def process_input(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False

    def update(self):
        self.world.update()

    def render(self):
        self.world.render(self.screen)
        self.fps_render.render(self.screen)
        pygame.display.flip()


if __name__ == "__main__":
    Game((960, 540)).loop()
